package com.spring.orm.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.orm.hibernate5.HibernateTemplate;

import com.spring.orm.entities.Student;

public class StudentDao {
	
	private HibernateTemplate hibernateTemplate;
	
	
	
	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}



	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	@Transactional
	//Insert student into table
	public int insert(Student student) {
		Integer i = (Integer)this.hibernateTemplate.save(student);
		return i;
	}
	
	
	//Get the Single Data(Object)
	public Student getStudent(int studentId) {
		Student student =hibernateTemplate.get(Student.class, studentId);
		return student;
	}
	
	//get all student(all rows)
	public List<Student> getAllStudents(){
	List<Student> students =	hibernateTemplate.loadAll(Student.class);
	return students;
	}
	
	@Transactional
	//delete a student from table
	public void deleteStudent(int studentId) {
		Student student =hibernateTemplate.get(Student.class,studentId);
		hibernateTemplate.delete(student);
	}
	
	@Transactional
	//Update Student
	public void updateStudent(Student student) {
		hibernateTemplate.update(student);
	}
	
	
	

}
