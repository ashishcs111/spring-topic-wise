package com.ashish.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class SearchController {
	
	@RequestMapping("/home")
	public String home() {
		System.out.println("going to home");
		return "home";
	}
	
	@RequestMapping("/search")
	public RedirectView search(@RequestParam("searchbox") String query) {
		RedirectView redirectView = new RedirectView();
		String url = "https://www.google.com/search?q="+query;
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("frame",url);
		redirectView.setUrl(url);
		return redirectView;
		
	}


}
