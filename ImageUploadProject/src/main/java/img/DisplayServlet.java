package img;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DisplayServlet
 */
@WebServlet("/DisplayServlet")
public class DisplayServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplayServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String imageId = request.getParameter("imageId");
		System.out.println(imageId);
		int imgId = 0;
		String imgFileName  = null;
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3308/imageupload","root","1234");
			String qr = "select * from image;";
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(qr);
			while(rs.next()) {
				if(rs.getInt("imageId") == Integer.parseInt(imageId))
				{
					System.out.println("running");
					imgId = rs.getInt("imageId");
					imgFileName = rs.getString("imagefilename");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		request.setAttribute("id", String.valueOf(imgId));
		request.setAttribute("img", imgFileName);
		request.getRequestDispatcher("displayImage.jsp").forward(request, response);
		
		
	}

}
