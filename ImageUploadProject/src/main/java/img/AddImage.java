package img;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 * Servlet implementation class AddImage
 */
@MultipartConfig
@WebServlet("/AddImage")
public class AddImage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddImage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("servlet is running");
		Part file = request.getPart("image");
		
		String imageFileName = file.getSubmittedFileName();
		System.out.println("selected file name is : "+ imageFileName);
		
		String uploadPath = "C:/Users/hp/eclipse-workspace/ImageUploadProject/src/main/webapp/images/"+imageFileName;
		
		System.out.println(uploadPath);
		
		try {
			FileOutputStream fos = new FileOutputStream(uploadPath);
			InputStream is = file.getInputStream();
			byte[] data = new byte[is.available()];
			is.read(data);
			fos.write(data);
			fos.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3308/imageupload","root","1234");
			String qr = "insert into image(imageFileName) values(?)";
			PreparedStatement pt = con.prepareStatement(qr);
			pt.setString(1, imageFileName);
			
			int rows = pt.executeUpdate();
			
			if(rows>0) {
				System.out.println("Image Uploaded");
			}
			else
			{
				System.out.println("failed to upload image");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
