package com.example.iteration1_looseCoupling.game;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("mongodbdatabase")
public class MongodbDatabase implements DataService {

	public int data() {
		int arr[]= {10,20,30,40,50};
		return arr[1];
	}
	
	public int max() {
		return 50;
	}
	
}
