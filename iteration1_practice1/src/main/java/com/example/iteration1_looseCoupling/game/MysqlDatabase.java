package com.example.iteration1_looseCoupling.game;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Qualifier("mysqldatabase")
@Primary
public class MysqlDatabase implements DataService{

	public int data() {
		int arr[]= new int[]{60,70,80,90,100};
		return arr[1];
	}
	
	public int max() {
		return 100;
	}
	
}
