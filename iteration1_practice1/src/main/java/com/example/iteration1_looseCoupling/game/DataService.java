package com.example.iteration1_looseCoupling.game;

public interface DataService {

	public int data();
	
	public int max();
}
