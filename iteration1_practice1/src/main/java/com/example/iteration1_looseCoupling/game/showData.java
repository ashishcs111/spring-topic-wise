package com.example.iteration1_looseCoupling.game;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class showData {
	
	private DataService dataService;

	@Autowired
	public void showData(DataService dataservice) {
		this.dataService = dataservice;
	}
	
	public void run() {
		System.out.println(dataService.data());
		System.out.println(dataService.max());
	}
}
