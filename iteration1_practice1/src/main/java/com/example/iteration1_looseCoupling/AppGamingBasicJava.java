package com.example.iteration1_looseCoupling;

import java.util.Arrays;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.example.iteration1_looseCoupling.game.DataService;
import com.example.iteration1_looseCoupling.game.showData;


@Configuration
@ComponentScan
public class AppGamingBasicJava {
	

	public static void main(String[] args) {
		
		try (var context = new AnnotationConfigApplicationContext(AppGamingBasicJava.class)) {
			Arrays.stream(context.getBeanDefinitionNames()).forEach(System.out::println);
			
			context.getBean(showData.class).run();
		}
	}

}
