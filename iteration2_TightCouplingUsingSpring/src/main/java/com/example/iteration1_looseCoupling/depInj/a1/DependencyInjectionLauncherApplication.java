package com.example.iteration1_looseCoupling.depInj.a1;

import java.util.Arrays;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
class YourBusinessClass{
	/*
	 * THis is dependency Injection using Fields
	@Autowired
	Dependency1 dependency1;
	@Autowired
	Dependency2 dependency2;
	*/
	
	
	Dependency1 dependency1;
	Dependency2 dependency2;
	
	
	//THis is Dependency Injection using constructor
	@Autowired   //autowired is not required for cons
	public YourBusinessClass(Dependency1 dependency1, Dependency2 dependency2) {
		super();
		System.out.println("Constructor Injection- YourBusiness Class");
		this.dependency1 = dependency1;
		this.dependency2 = dependency2;
	}


	/*
	 * THis is Dependency Injection through setters and getters
	 * 
	@Autowired
	public void setDependency1(Dependency1 dependency1) {
		this.dependency1 = dependency1;
	}



	@Autowired
	public void setDependency2(Dependency2 dependency2) {
		this.dependency2 = dependency2;
	}
	*/




	public String toString() {
		return "Using "+dependency1+" and "+dependency2;
	}
}

@Component
class Dependency1{
	
}

@Component
class Dependency2{
	
} 


@Configuration
@ComponentScan("com.example.iteration1_looseCoupling.depInj.a1")
public class DependencyInjectionLauncherApplication {

	public static void main(String[] args) {
		try (var context = new AnnotationConfigApplicationContext(DependencyInjectionLauncherApplication.class)) {
				
			
			Arrays.stream(context.getBeanDefinitionNames()).forEach(System.out::println);
		
			System.out.println(context.getBean(YourBusinessClass.class));
		} catch (BeansException e) {
			e.printStackTrace();
		}
		
	}

}
		