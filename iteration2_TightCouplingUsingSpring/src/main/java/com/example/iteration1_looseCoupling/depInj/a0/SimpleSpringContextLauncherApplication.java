package com.example.iteration1_looseCoupling.depInj.a0;

import java.util.Arrays;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
class BusinessService{
	private DataService dataservice;

	@Autowired
	public DataService getDataservice() {
		System.out.println("Setter Injection");
		return dataservice;
	}
	public void setDataservice(DataService dataservice) {
		this.dataservice = dataservice;
	}
	
	
}

@Component
class DataService{
	
}





@Configuration
@ComponentScan("com.example.iteration1_looseCoupling.depInj.a1")
public class SimpleSpringContextLauncherApplication {
 
	public static void main(String[] args) {
		try (var context = new AnnotationConfigApplicationContext(SimpleSpringContextLauncherApplication.class)) {
				
			
			Arrays.stream(context.getBeanDefinitionNames()).forEach(System.out::println);
		
		} catch (BeansException e) {
			e.printStackTrace();
		}
		
	}

}
		