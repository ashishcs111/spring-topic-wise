package com.example.iteration1_looseCoupling;

import java.util.Arrays;

import org.springframework.beans.BeansException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App03GamingSpringBeans {

	public static void main(String[] args) {
		//Launch a Spring Context
		//Configure the things that we want Spring to manage
		
		
		try (var context = new AnnotationConfigApplicationContext(GamingConfiguration.class)) {
			context.getBean(GamingConsole.class).up(); //Retrieving beans managed by Spring
			context.getBean(GameRunner.class).run();
			
		
		} catch (BeansException e) {
			e.printStackTrace();
		}
		
	}

}
		