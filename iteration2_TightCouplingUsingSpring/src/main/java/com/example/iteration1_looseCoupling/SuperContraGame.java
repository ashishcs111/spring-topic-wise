package com.example.iteration1_looseCoupling;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Qualifier("SuperContraGame")
public class SuperContraGame implements GamingConsole {

	public void up() {
		System.out.println("up");	
	}
	
	public void down() {
		System.out.println("Sit Down");	
	}
	
	public void left() {
		System.out.println("go back");	
	}
	
	public void right() {
		System.out.println("Shoot a Bullet");	
	}
}
