package com.example.iteration1_looseCoupling.lazyInit;

import java.util.Arrays;

import org.springframework.beans.BeansException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;



@Component
class ClassA{
	
}



@Component
@Lazy(value = true)
class ClassB{
	private ClassA classA;
	
	public ClassB(ClassA classA) {
		System.out.println("Some initialization Logic");
		this.classA= classA;
	}

	public void doSomething() {
		System.out.println("DO Something");
	}
}

@Configuration
@ComponentScan
public class LazyInitializationLauncherApplication {
 
	public static void main(String[] args) {
		try (var context = new AnnotationConfigApplicationContext(LazyInitializationLauncherApplication.class)) {
			
			System.out.println("Initialization of context in done");
			
			Arrays.stream(context.getBeanDefinitionNames()).forEach(System.out::println);
		
			context.getBean(ClassB.class).doSomething();
		} catch (BeansException e) {
			e.printStackTrace();
		}
		
	}

}
		