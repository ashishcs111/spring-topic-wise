package com.example.iteration1_looseCoupling.CDI;

import java.util.Arrays;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import jakarta.inject.Inject;
import jakarta.inject.Named;

//@Component
@Named      //this is the annotation provided by jakarta
class BusinessService{
	private DataService dataservice;
  
	public DataService getDataservice() {
		
		return dataservice;
	}
	
	//@Autowired
	@Inject
	public void setDataservice(DataService dataservice) {
		this.dataservice = dataservice;
		System.out.println("Setter Injection");
	}
	
	
}

//@Component
@Named
class DataService{
	
}





@Configuration
@ComponentScan
public class SimpleSpringContextLauncherApplication {
 
	public static void main(String[] args) {
		try (var context = new AnnotationConfigApplicationContext(SimpleSpringContextLauncherApplication.class)) {
				
			
			Arrays.stream(context.getBeanDefinitionNames()).forEach(System.out::println);
		
		} catch (BeansException e) {
			e.printStackTrace();
		}
		
	}

}
		