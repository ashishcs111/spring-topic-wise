package com.example.iteration1_looseCoupling;

public interface GamingConsole {

	void up();
	void down();
	void left();
	void right();
}
