package com.example.iteration1_looseCoupling.xmlConfig;

import java.util.Arrays;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
//THis is ABout XML configuration in spring


public class SimpleSpringContextLauncherApplication {
 
	public static void main(String[] args) {
		try (var context = new ClassPathXmlApplicationContext("contextConfiguration.xml")) {
				
			
			Arrays.stream(context.getBeanDefinitionNames()).forEach(System.out::println);
		
			
			System.out.println(context.getBean("name"));
			System.out.println(context.getBean("age"));
		} catch (BeansException e) {
			e.printStackTrace();
		}
		
	}

}
		