package com.example.iteration1_looseCoupling;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
@ComponentScan("com.example.iteration1_looseCoupling")
public class GamingConfiguration {



	/*
	 * @Bean public GamingConsole game() { var game = new PackmanGame(); return
	 * game; }
	 */

	/*
	 * @Bean public GamingConsole game() { var game = new SuperContraGame(); return
	 * game; }
	 */

	/*
	 * @Bean public GamingConsole game3() { var game = new MarioGame(); return game;
	 * }
	 */

	@Bean
	public GameRunner gameRunner(GamingConsole game){
		var gameRunner = new GameRunner(game);
		return gameRunner;
	}

} 
