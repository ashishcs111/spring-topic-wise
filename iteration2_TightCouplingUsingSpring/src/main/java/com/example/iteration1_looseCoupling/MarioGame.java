package com.example.iteration1_looseCoupling;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
@Qualifier("MarioGame")
public class MarioGame implements GamingConsole {

	public void up() {
		System.out.println("jump");	
	}
	
	public void down() {
		System.out.println("GO into a Hole");	
	}
	
	public void left() {
		System.out.println("go back");	
	}
	
	public void right() {
		System.out.println("Accelerate");	
	}
}
