package com.example.iteration1_looseCoupling.helloworld;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

record Person (String name,int age,Address address) {}; //this will create a class and setter getter for the parameter variables
record Address (String firstLine, String city) {}; 

@Configuration
public class HelloWorldConfiguration {

	@Bean
	public String name() {
		return "ashish";
	}
	
	@Bean
	public int age() {
		return 21;
	}
	
	@Bean
	public Person person() {
		var person = new Person("Ravi", 20, new Address("Main Street","Utrecht"));
		return person;
	}
	
	@Bean
	public Person person2MethodCall() {
		var person2 = new Person(name(), age(),address());
		return person2;
	}
	
	@Bean
	public Person person3Parameters(String name,int age,@Qualifier("addressQualifier") Address address) {
		var person2 = new Person(name, age,address);
		return person2;
	}
	
	@Bean(name="address")
	@Primary         //If there are two beans with same name then primary will be execute first
	public Address address() {
		var address = new Address("vijay Nagar","Indore");
		return address;
	}
	
	@Bean
	@Qualifier("addressQualifier")  //this will execute according to the name
	public Address address2() {
		var address = new Address("vijay Nagar","Indore");
		return address;
	}
	
	
	
}
