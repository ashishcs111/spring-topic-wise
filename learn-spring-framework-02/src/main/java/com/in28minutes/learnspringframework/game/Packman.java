package com.in28minutes.learnspringframework.game;

import org.springframework.stereotype.Component;

@Component
public class Packman implements GamingConsole {

	public void up() {
		System.out.println("move up");
		
	}

	public void down() {
		System.out.println("move down");
		
	}

	public void left() {
		System.out.println("move forward");
		
	}

	public void right() {
		System.out.println("Move backward");
		
	}

	
}
