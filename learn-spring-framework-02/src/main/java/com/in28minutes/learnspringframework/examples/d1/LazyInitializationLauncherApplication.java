package com.in28minutes.learnspringframework.examples.d1;

import java.util.Arrays;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;



@Component
class ClassA{
	
}

@Component
@Lazy
class ClassB{
	
	private ClassA classA;
	
	public ClassB(ClassA classA) {
		//logic
		System.out.println("Some initialization logic");
		this.classA = classA;
	}
}

//YourBusinessClass
@Component
class YourBusinessClass{
	
	@Autowired
	Dependency1 dependency1;
	
	@Autowired
	Dependency2 dependency2;
	
	
	/*
	public Dependency1 getDependency1() {
		return dependency1;
	}

	@Autowired
	public void setDependency1(Dependency1 dependency1) {
		this.dependency1 = dependency1;
	}

	public Dependency2 getDependency2() {
		return dependency2;
	}

	@Autowired
	public void setDependency2(Dependency2 dependency2) {
		this.dependency2 = dependency2;
	}
	*/

	@Autowired
	public YourBusinessClass(Dependency1 dependency1, Dependency2 dependency2) {
		super();
		this.dependency1 = dependency1;
		this.dependency2 = dependency2;
		System.out.println("Constructor Injection - yourBusinessClass");
	}

	public String toString() {
		return "using"+ dependency1 + " and "+ dependency2;
	}
}
//Dependency1
@Component
class Dependency1{
	
}
//Dependency2
@Component
class Dependency2{
	
}

@Configuration
@ComponentScan
public class LazyInitializationLauncherApplication {

	public static void main(String[] args) {
		
		try (var context = new AnnotationConfigApplicationContext(LazyInitializationLauncherApplication.class)) {
		
		Arrays.stream(context.getBeanDefinitionNames()).forEach(System.out::println);
		
		
		System.out.println(context.getBean(YourBusinessClass.class));
		} catch (BeansException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
 