package com.example.iteration1_looseCoupling;

import java.util.Scanner;

import com.example.iteration1_looseCoupling.game.GameRunner;
import com.example.iteration1_looseCoupling.game.GamingConsole;
import com.example.iteration1_looseCoupling.game.MarioGame;
import com.example.iteration1_looseCoupling.game.PackmanGame;
import com.example.iteration1_looseCoupling.game.SuperContraGame;

public class AppGamingBasicJava {
	
	public static  GamingConsole games(int num) {
		switch(num) {
		case 1:
			return new PackmanGame();
			
		case 2:
			return new SuperContraGame();
		
		case 3:
			return new MarioGame();
		}
		return null;
		
	}

	public static void main(String[] args) {
		
		//var game = new MarioGame();
		//var game = new SuperContraGame()
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		
		var game = AppGamingBasicJava.games(num);
		var gameRunner = new GameRunner(game);
		gameRunner.run();
 
	}

}
		