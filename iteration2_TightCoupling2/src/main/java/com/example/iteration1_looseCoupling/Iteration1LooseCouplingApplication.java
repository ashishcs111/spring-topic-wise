package com.example.iteration1_looseCoupling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Iteration1LooseCouplingApplication {

	public static void main(String[] args) {
		SpringApplication.run(Iteration1LooseCouplingApplication.class, args);
	}

}
