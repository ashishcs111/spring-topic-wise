package com.example.iteration1_looseCoupling.game;

public interface GamingConsole {

	void up();
	void down();
	void left();
	void right();
}
