package com.springjpa.jpa;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.springjpa.entity.Person;

//Repository
//Transaction
@Repository
@Transactional
public class PersonJpaRepository {

	//Connect to the database
	@PersistenceContext
	EntityManager entityManger;
	
	public Person findById(int id) {
		 
		return entityManger.find(Person.class,  id);
	}
	
}
