package com.ashish.stereotype;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class StereotypeTest {

	public static void main(String[] args) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("com/ashish/stereotype/config.xml");
		
		Student student = context.getBean("student", Student.class);
		System.out.println(student);
		
		
	
	}
}
