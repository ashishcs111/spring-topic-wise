package com.ashish.springexpressionlang;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpELTest {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("com/ashish/springexpressionlang/config.xml");
	
		Demo demo = context.getBean("demo",Demo.class);
		System.out.println(demo);
	}
}
