package com.ashish.autowiring.annotaions;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AutowireTest {

	public static void main(String[] args) {
		
	ApplicationContext context =new ClassPathXmlApplicationContext("com/ashish/autowiring/annotaions/config.xml");
	
	//by using Emp.class in the argument it will automatically create the object of Emp type no need of type casting
	Emp emp = context.getBean("emp1",Emp.class);
	System.out.println(emp);
	
	}
}
