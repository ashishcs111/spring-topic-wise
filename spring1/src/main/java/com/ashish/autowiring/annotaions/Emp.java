package com.ashish.autowiring.annotaions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class Emp {
	@Autowired
	@Qualifier("address2")
	private Address address;
	
	public Emp() {}

	public Address getAddress() {
		return address;
	}

//	@Autowired
	
	public void setAddress(Address address) {
		System.out.println("value is set");
		this.address = address;
	}

//	@Autowired
	public Emp(Address address) {
		super();
		System.out.println("constructor is used");
		this.address = address;
	}

	@Override
	public String toString() {
		return "Emp [] "+address;
	}
	
	

}
