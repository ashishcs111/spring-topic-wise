package com.ashish.lifeCycle;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class LifeCycleTest {

	public static void main(String[] args) {
		
//	ApplicationContext context =new ClassPathXmlApplicationContext("com/ashish/lifeCycle/config.xml");
	
	AbstractApplicationContext context =new ClassPathXmlApplicationContext("com/ashish/lifeCycle/config.xml");	
		
	Samosa samosa = (Samosa) context.getBean("samosa");
	System.out.println(samosa);
	//this will register the shutdown hook and execute the destroy method before bean destruction
	context.registerShutdownHook();
	
	
	
	Pepsi pepsi = (Pepsi)context.getBean("pepsi");
	System.out.println(pepsi);
	
	}
}
