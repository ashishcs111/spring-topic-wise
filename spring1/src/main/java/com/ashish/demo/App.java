package com.ashish.demo;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

	public static void main(String[] args) {
		ApplicationContext context =new ClassPathXmlApplicationContext("com/ashish/demo/config.xml");
		
		Student student = (Student)context.getBean("student1");
		System.out.println(student);
		
		
		Student student2 = (Student)context.getBean("student2");
		System.out.println(student2);
		
		Student student3 = (Student)context.getBean("student3");
		System.out.println(student3);
		
		
		listOfFive listSetMap = (listOfFive) context.getBean("collections");
		System.out.println(listSetMap);
		
		A a = (A)context.getBean("a");
		System.out.println(a);
		
		B b = (B)context.getBean("b");
		System.out.println(b);
		
		Person person = (Person)context.getBean("person");
		System.out.println(person);
		
	}
}
