package com.ashish.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {
	
//	@Autowired
//	private EmpDao empDao;
//	
//	@Autowired
//	public HomeController() {
//		this.empDao = empDao;
//	}
	
	@RequestMapping("/home")
	public String home() {
		System.out.println("home");
		return "home";
	}
	
//	@RequestMapping("/addEmp")
//	public String addEmp() {
//		System.out.println("home");
//		return "addEmp";
//	}
//	
//	@RequestMapping(path = "/createEmp",method = RequestMethod.POST)
//	public String createEmp(@ModelAttribute Emp emp) {
//		System.out.println(emp);
//		empDao.saveEmp(emp);
//		return "addEmp";
//	}

}
