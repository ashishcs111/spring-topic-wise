package com.in28minutes.learnspringframework.databases;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class DataServiceClass {
	
	private DataServiceInterface data;

	public DataServiceClass( DataServiceInterface data) {
		this.data = data;
	}
	
	public void operation() {
		
		System.out.println("Operations started on data");
		data.show();
		data.FirstLetter();
	}
	
	

}
