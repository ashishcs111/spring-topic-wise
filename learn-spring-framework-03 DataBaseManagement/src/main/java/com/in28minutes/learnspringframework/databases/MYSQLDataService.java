package com.in28minutes.learnspringframework.databases;


import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("mysql")
public class MYSQLDataService implements DataServiceInterface {
	

	public MYSQLDataService() {
		
	}

	public void show() {
		
			System.out.println("This is Mysql database");
		
	}

	public void FirstLetter() {
		int arr[]= new int[] {1,2,3,4,5,6};
		for(int i=0;i<arr.length;i++)
			System.out.println(arr[i]);
		
	}
}
