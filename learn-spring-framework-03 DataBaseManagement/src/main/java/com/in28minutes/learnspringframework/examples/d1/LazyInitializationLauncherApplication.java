package com.in28minutes.learnspringframework.examples.d1;

import java.sql.Array;
import java.util.Arrays;

import org.springframework.beans.BeansException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import com.in28minutes.learnspringframework.databases.DataServiceClass;

@Component
@ComponentScan("com.in28minutes.learnspringframework.databases")
public class LazyInitializationLauncherApplication {
	
	public static void main(String[] args) {
		
		try (var context = new AnnotationConfigApplicationContext(LazyInitializationLauncherApplication.class)) {
			context.getBean(DataServiceClass.class).operation();
			
			Arrays.stream(context.getBeanDefinitionNames()).forEach(System.out::println);
		} catch (BeansException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
