package com.in28minutes.learnspringframework.examples.g1;

import java.sql.Array;
import java.util.Arrays;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import com.in28minutes.learnspringframework.databases.DataServiceClass;

import jakarta.inject.Inject;
import jakarta.inject.Named;


//@Component
@Named
class BusinessService{
	private Dataservice dataservice;

	
	public Dataservice getDataservice() {
		System.out.println("Setter injection");
		return dataservice;
	}

	//@Autowired
	@Inject
	public void setDataservice(Dataservice dataservice) {
		this.dataservice = dataservice;
	}
	
	
}

//@Component
@Named
class Dataservice{
	
} 

@Component 
@ComponentScan
public class CDIContextLauncherApplication {
	
	public static void main(String[] args) {
		
		try (var context = new AnnotationConfigApplicationContext(CDIContextLauncherApplication.class)) {
			
			Arrays.stream(context.getBeanDefinitionNames()).forEach(System.out::println);
			System.out.println(context.getBean(BusinessService.class).getDataservice());
		} catch (BeansException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
