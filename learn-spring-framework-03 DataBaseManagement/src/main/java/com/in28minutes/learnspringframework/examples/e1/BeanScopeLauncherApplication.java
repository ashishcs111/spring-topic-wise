package com.in28minutes.learnspringframework.examples.e1;

import java.sql.Array;
import java.util.Arrays;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.in28minutes.learnspringframework.databases.DataServiceClass;



@Component
class NormalClass{
	
}


@Scope(value=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Component
class PrototyoeClass{
	
}

@Component
@ComponentScan("com.in28minutes.learnspringframework.examples.e1.")
public class BeanScopeLauncherApplication {
	
	public static void main(String[] args) {
		
		try (var context = new AnnotationConfigApplicationContext(BeanScopeLauncherApplication.class)) {
			
			
			System.out.println(context.getBean(NormalClass.class));
			System.out.println(context.getBean(NormalClass.class));
			
			System.out.println(context.getBean(PrototyoeClass.class));
			System.out.println(context.getBean(PrototyoeClass.class));
			System.out.println(context.getBean(PrototyoeClass.class));
			
			
			
			
		} catch (BeansException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
