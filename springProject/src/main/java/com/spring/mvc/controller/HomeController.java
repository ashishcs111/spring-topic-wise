package com.spring.mvc.controller;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@ComponentScan("com.spring.mvc.controller")
public class HomeController {

	@RequestMapping(path="/home")
	public String home() {
		return "home.jsp";
	}
	
}
