package com.ashish.Entity;
import javax.persistence.*;

@Entity
@Table(name = "emp_details")
public class Emp {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String name;
	private String address;
	private String email;
	private String password;
	private String designation;
	private String sallery;
	
	
	
	
	public Emp() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public Emp(int id, String name, String address, String email, String password, String designation, String sallery) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.email = email;
		this.password = password;
		this.designation = designation;
		this.sallery = sallery;
	}

	

	public String getSallery() {
		return sallery;
	}


	public void setSallery(String sallery) {
		this.sallery = sallery;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	@Override
	public String toString() {
		return "Emp [id=" + id + ", name=" + name + ", address=" + address + ", email=" + email + ", password="
				+ password + ", designation=" + designation + ", sallery=" + sallery + "]";
	}
	
	
	

}
