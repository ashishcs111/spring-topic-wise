package com.ashish.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.ashish.Entity.Emp;


@Repository
public class EmpDaoImpl implements EmpDao {
	
	@Autowired
	private HibernateTemplate hibernateTemplate;
	
	

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	@Transactional
	public int saveEmp(Emp emp) {
		Integer i = (Integer)hibernateTemplate.save(emp);
		return i;
	}
	@Transactional
	public Emp getEmpById(int id) {
	 Emp emp = (Emp)hibernateTemplate.get(Emp.class, id);
		return emp;
	}
	@Transactional
	public List<Emp> getAllEmp() {
	List<Emp> list =	hibernateTemplate.loadAll(Emp.class);
		return list;
	}

	@Transactional
	public void update(Emp emp) {
		hibernateTemplate.update(emp);
		
	}

	@Transactional
	public void delete(int id) {
		Emp emp = hibernateTemplate.get(Emp.class, id);
		hibernateTemplate.delete(emp);
		
	}

}
