package com.spring.jdbc.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.spring.jdbc.entities.Student;

//@Component("studentDao")
public class StudentDaoImpl implements StudentDao {
	
//	@Autowired
	private JdbcTemplate jdbcTemplate;
	

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}


	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}


	@Override
	public int insert(Student student) {
		String query = "insert into student(id,name,city) values(?,?,?)";
		int result = jdbcTemplate.update(query,student.getIs(),student.getName(),student.getCity());
		return result;
	}


	@Override
	public int change(Student student) {
		String query = "update student set name=?, city=? where id=?";
		jdbcTemplate.update(query, student.getName(), student.getCity(),student.getIs() );
		return 0;
	}


	@Override
	public int delete(int studentId) {
		String query = "delete from student where id= ?";
		int result = jdbcTemplate.update(query,studentId);
		return result;
	}


	@Override
	public Student getStudent(int studentId) {
		String query = "select * from student where id =?";
		RowMapper<Student> rowMapper = new RowMapperImpl();
		Student student = jdbcTemplate.queryForObject(query, rowMapper,studentId);
		return student;
	}


	@Override
	public List<Student> getAllStudents() {
		String query = "select * from student";
		List<Student> students =jdbcTemplate.query(query, new RowMapperImpl() );
		return students;
	}

}
