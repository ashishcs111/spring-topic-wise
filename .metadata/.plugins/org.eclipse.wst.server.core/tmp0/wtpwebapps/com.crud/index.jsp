<html>
<head>
<title>Home</title>
 <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
<div class="container mt-2"><nav class="navbar navbar-expand-lg navbar-light bg-info" style="border-radius:20px;">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="home">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="contact">contact</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="show">Show</a>
        </li>
      </ul>
    </div>
  </div>
</nav></div>


<h2 class="text-center">Hello World!</h2>

<div class="container">
<div class="card mx-auto mt-5 bg-info" style="width:50%">
<h3 class="mx-auto text-center text-white my-2" style="text-transform:uppercase">Search Box</h3>

<form class="mt-3 container" action="search" method="post">

<div class="form-group ">

<input type="text" name="query" placeholder="write your query here" class="form-control"/>
</div>
<div class="container text-center my-2">
<button class="btn btn-outline-light">Search</button>
</div>


</form>
</div>

</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>
