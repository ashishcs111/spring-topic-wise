<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Insert title here</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
	crossorigin="anonymous">
</head>
<body>
<div class="container mt-2"><nav class="navbar navbar-expand-lg navbar-light bg-info" style="border-radius:20px;">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="home">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="contact">contact</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="show">Show</a>
        </li>
        <li class="nav-item">
       <form action="searchstu" ><input type="number" name="id" style="border-radius:20px; height:35px;"/><button class="btn btn-danger" style="border-radius:20px; margin-left:-20px;"> Search</button></form>   
        </li>
      </ul>
    </div>
  </div>
</nav></div>
<div class="container">
		
				<table class="table table-striped table-primary mx-auto my-5">
					<thead>
						<tr>
							<th>Id</th>
							<th>Name</th>
							<th>Email</th>
							<th>Password</th>
							<th>Delete</th>
							<th>Update</th>
						</tr>
					</thead>
					<tbody>
					<c:forEach var="i" items="${list }" ><tr>
					<form action="update" method="post">
					<td>${i.id }
						<input type="hidden" class="form-control" name="id"
								value="${i.id }">
						</td>
						<td>
							<input
								type="text" class="form-control" name="name"
								value="${i.name }" />
						</td>
						<td>
							<input
								type="text" class="form-control" name="email"
								value="${i.email }">
						</td>
						<td>
							<input
								type="text" class="form-control" name="pwd"
								value="${i.pwd }">
						</td>
						<td>
							<input
								type="submit" class="form-control" 	value="Update">
						</td>
						</form>
						<td>
							<a class="btn btn-danger" href="delete?id=${i.id }">Delete</a>
						</td>
						
						</tr></c:forEach>
					</tbody>
				</table>
			</div>


	<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
		crossorigin="anonymous"></script>
</body>
</html>