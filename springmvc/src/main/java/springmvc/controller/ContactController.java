package springmvc.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import springmvc.model.User;
import springmvc.service.UserService;

@Controller
public class ContactController {
	
	private UserService userService;
	
	@ModelAttribute
	public void CommonDataForModel(Model m) {
		m.addAttribute("Header", "Learn Coding");
		m.addAttribute("Desc", "Spring Mvc Framework");
	}
	
	@RequestMapping("/contact")
	public String showForm(Model m) {
//		m.addAttribute("Header", "Learn Coding");
//		m.addAttribute("Desc", "Spring Mvc Framework");
		return "contact";
	}
	
	@RequestMapping(path="/processform" , method = RequestMethod.POST)
//	public String handleForm(@RequestParam("email") String email, @RequestParam("username") String username, @RequestParam("password") String password, Model model) {
	public String handleForm(@ModelAttribute User user, Model model) {
		
		
		//Method 1 of processing data
//		System.out.println(email+" "+ password + " " + username);
//		model.addAttribute("username", username);
//		model.addAttribute("password", password);
//		model.addAttribute("email", email);
		
		
		
		//method 2 of processing data
//		User user = new User();
//		user.setEmail(email);
//		user.setPassword(password);
//		user.setUsername(username);
//		System.out.println(user);
//		model.addAttribute("user", user);
		
		
		//Method 3 of processing data is currently we are doing
		
		
		//Business Logic to send form data to database
		
		this.userService.createUser(user);
		return "success";
	}

}
