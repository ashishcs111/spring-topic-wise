package com.ashish.service;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.ashish.dao.EmpDao;
import com.ashish.entity.Emp;

public class DataServiceImplementation implements DataService {

	ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
	
	EmpDao empDao = (EmpDao)context.getBean("ddd");
	
	
	public void registerAnUser(Emp emp) {	
		empDao.saveEmp(emp);
	}

}
