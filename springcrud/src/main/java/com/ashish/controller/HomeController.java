package com.ashish.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ashish.dao.EmpDao;
import com.ashish.entity.Emp;
import com.ashish.service.DataService;

@Controller
public class HomeController {
	
	@Autowired
	DataService dataService;
	
	@RequestMapping("/home")
	public String home() {
		System.out.println("home");
		return "home";
	}
	
	@RequestMapping("/addEmp")
	public String addEmp() {
		System.out.println("home");
		return "addEmp";
	}
	
	@RequestMapping(path = "/createEmp",method = RequestMethod.POST)
	public String createEmp(@ModelAttribute Emp emp) {
		System.out.println(emp);
		dataService.registerAnUser(emp);
		return "addEmp";
	}

}
